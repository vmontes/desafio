#include "data_manager.h"
#include "stock.h"

int main()
{
	const data_manager dm;
	const stock st;

	std::shared_ptr<mov_items_t> mov_items = dm.read_moviments_file("D:\\Project\\gitlab\\desafio\\data\\MovtoITEM.csv");

	std::shared_ptr<balance_items_t> balance_items = dm.read_balance_file("D:\\Project\\gitlab\\desafio\\data\\SaldoITEM.csv");

	std::shared_ptr<summary_data_t> summary = st.build_day_summary(mov_items, balance_items);

	st.print_summary(summary, "D:\\Project\\gitlab\\desafio\\data\\summary.csv");

	return 0;
}