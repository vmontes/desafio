#pragma once

#include "data_manager.h"

#include <memory>
#include <string>
#include <unordered_map>
#include <map>
#include <string>

struct day_summary_item_t : public date_format_t
{
	double input_amount;
	double input_value;
	double output_amount;
	double output_value;
	double init_total_amount;
	double init_total_value;
	double final_total_amount;
	double final_total_value;
};

struct summary_item_t
{
	std::string item;
	balance_item_data_t balance_item;
	std::map<int /*unix timestamp*/, day_summary_item_t> day_sum;

	// changing values

	double last_amount;
	double last_value;
};

typedef std::unordered_map<std::string/*item*/, summary_item_t> summary_data_t;

class stock
{
public:

	stock() = default;
	~stock() = default;

	std::shared_ptr<summary_data_t> build_day_summary(std::shared_ptr<mov_items_t> mov_items,
		std::shared_ptr<balance_items_t> balance_items) const;

	void print_summary(std::shared_ptr<summary_data_t> summary, std::string filename) const;
};

