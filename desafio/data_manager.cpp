#include "data_manager.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <memory>
#include <assert.h>

//#define VERBOSE

using namespace std;

enum MOV_INPUT_FIELDS
{
   MOV_ITEM,
   MOV_TYPE,
   MOV_DATE,
   MOV_AMOUNT,
   MOV_VALUE
};

enum BALANCE_INPUT_FIELDS
{
    BALANCE_ITEM,
    BALANCE_INIT_DATE,
    BALANCE_INIT_AMOUNT,
    BALANCE_INIT_VALUE,
    BALANCE_FINAL_DATE,
    BALANCE_FINAL_AMOUNT,
    BALANCE_FINAL_VALUE
};

void check_header_mov(std::string input)
{
    mov_input_line_t line_data;
    string field;
    std::istringstream split(input);

    int n = 0;
    while (getline(split, field, ';'))
    {
        switch (n++)
        {
            case MOV_ITEM:
                assert("item" == field);
                break;
            case MOV_TYPE:
                assert("tipo_movimento" == field);
                break;
            case MOV_DATE:
                assert("data_lancamento" == field);
                break;
            case MOV_AMOUNT:
                assert("quantidade" == field);
                break;
            case MOV_VALUE:
                assert("valor" == field);
                break;
        }
    }
}

mov_input_line_t build_line_mov(std::string input)
{
    mov_input_line_t line_data;
    string field;
    std::istringstream split(input);

    int n = 0;
    while (getline(split, field, ';'))
    {
        switch (n++)
        {
            case MOV_ITEM:
                line_data.item = field;
             break;
            case MOV_TYPE:
                line_data.type = field;
            break;
            case MOV_DATE:
                line_data.date = field;
            break;
            case MOV_AMOUNT:
                line_data.amount = field;
            break;
            case MOV_VALUE:
                line_data.value = field;
            break;
        }
    }
    
    return line_data;
}

shared_ptr<mov_items_t> data_manager::read_moviments_file(std::string file) const
{
    cout << "Reading moviments file: " << file << endl;
    shared_ptr<mov_items_t> items = make_shared<mov_items_t>();
    string line;
    ifstream csv_file(file.c_str());
    if (csv_file)
    {
        if (getline(csv_file, line))
            check_header_mov(line);
        while (getline(csv_file, line))  
        {
            mov_input_line_t data = build_line_mov(line);
            mov_item_data_t item; 
            item.type = data.type;
            item.amount = data.get_amount();
            item.value = data.get_value();
            auto& mov = (*items)[data.item][data.get_ordering_date()];
            mov.date = data.date;
            mov.movs.push_back(item);
#ifdef VERBOSE
            std::cout << "Processing item:" << data.item << endl;
#endif
        }
        csv_file.close();
    }
    else 
        cout << "ERROR: cannot open input file\n";

    return items;
}

void check_header_balance(std::string input)
{
    mov_input_line_t line_data;
    string field;
    std::istringstream split(input);

    int n = 0;
    while (getline(split, field, ';'))
    {
        switch (n++)
        {
            case BALANCE_ITEM:
                assert("item" == field);
                break;
            case BALANCE_INIT_DATE:
                assert("data_inicio" == field);
                break;
            case BALANCE_INIT_AMOUNT:
                assert("qtd_inicio" == field);
                break;
            case BALANCE_INIT_VALUE:
                assert("valor_inicio" == field);
                break;
            case BALANCE_FINAL_DATE:
                assert("data_final" == field);
                break;
            case BALANCE_FINAL_AMOUNT:
                assert("qtd_final" == field);
                break;
            case BALANCE_FINAL_VALUE:
                assert("valor_final" == field);
                break;
        }
    }
}

balance_item_data_t build_line_balance(std::string input)
{
    balance_item_data_t line_data;
    string field;
    std::istringstream split(input);

    int n = 0;
    while (getline(split, field, ';'))
    {
        switch (n++)
        {
            case BALANCE_ITEM:
                line_data.item = field;
                break;
            case BALANCE_INIT_DATE:
                line_data.init_date = field;
                break;
            case BALANCE_INIT_AMOUNT:
                line_data.init_amount = field;
                break;
            case BALANCE_INIT_VALUE:
                line_data.init_value = field;
                break;
            case BALANCE_FINAL_DATE:
                line_data.final_date = field;
                break;
            case BALANCE_FINAL_AMOUNT:
                line_data.final_amount = field;
                break;
            case BALANCE_FINAL_VALUE:
                line_data.final_value = field;
                break;
        }
    }

    return line_data;
}

shared_ptr<balance_items_t> data_manager::read_balance_file(std::string file) const
{
    cout << "Reading balance file: " << file << endl;
    shared_ptr<balance_items_t> items = make_shared<balance_items_t>();
    string line;
    ifstream csv_file(file.c_str());
    if (csv_file)
    {
        if (getline(csv_file, line))
            check_header_balance(line);
        while (getline(csv_file, line))
        {
            balance_item_data_t data = build_line_balance(line);
            balance_item_data_t& item = (*items)[data.item];
            item = data;
#ifdef VERBOSE
            std::cout << "Processing item:" << data.item << endl;
#endif
        }
        csv_file.close();
    }
    else
        cout << "ERROR: cannot open input file\n";

    return items;
}