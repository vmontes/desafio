#pragma once
#include <string>
#include <unordered_map>
#include <map>
#include <memory>
#include <list>

struct date_format_t
{
	std::string date;

	// fix day month order
	std::string get_formated_date()
	{
		if (date.find_first_of("/") == 1)
			date = std::string("0") + date;
		if (date.find_last_of("/") == 4)
			date = date.substr(0, 3) + std::string("0") + date.substr(3);
		std::size_t m = date.find_first_of("/");
		std::size_t y = date.find_last_of("/");
		std::string ord_string = date.substr(m + 1, 2) + "/" + date.substr(0, m) + "/" + date.substr(y + 1, 4);
		return std::move(ord_string);
	}

	// fix date and get an ordering index of days
	int get_ordering_date()
	{
		if (date.find_first_of("/") == 1)
			date = std::string("0") + date;
		if (date.find_last_of("/") == 4)
			date = date.substr(0, 3) + std::string("0") + date.substr(3);
		std::size_t m = date.find_first_of("/");
		std::size_t y = date.find_last_of("/");
		std::string ord_string = date.substr(y + 1, 4) + date.substr(0, m) +date.substr(m + 1, 2);
		int n = std::stoi(ord_string);
		return n;
	}
};

struct mov_input_line_t : public date_format_t
{
	std::string item;
	std::string type;
	std::string amount;
	std::string value;

	double get_amount()
	{
		return std::stod(amount);
	}

	double get_value()
	{
		return std::stod(value);
	}
};

struct mov_item_data_t
{
	std::string type;
	double amount;
	double value;
};

struct mov_item_data_list_t
{
	std::string date;
	std::list<mov_item_data_t> movs;
};

typedef std::unordered_map<std::string /*item*/, std::map<int/*unix date*/, 
	mov_item_data_list_t>> mov_items_t;

struct balance_item_data_t
{
	std::string item;
	std::string init_date;
	std::string init_amount;
	std::string init_value;
	std::string final_date;
	std::string final_amount;
	std::string final_value;

	double get_init_amount()
	{
		return std::stod(init_amount);
	}

	double get_init_value()
	{
		return std::stod(init_value);
	}

};

typedef std::unordered_map<std::string /*item*/, balance_item_data_t> balance_items_t;


class data_manager
{
public:

	data_manager() = default;

	~data_manager() = default;

	std::shared_ptr<mov_items_t> read_moviments_file(std::string file) const;

	std::shared_ptr<balance_items_t> read_balance_file(std::string file) const;
};

