#include "stock.h"

#include <iostream>

using namespace std;

std::shared_ptr<summary_data_t> stock::build_day_summary(std::shared_ptr<mov_items_t> mov_items,
	std::shared_ptr<balance_items_t> balance_items) const
{
	cout << "Processing day summaries..." << endl;
	std::shared_ptr<summary_data_t> summary = make_shared<summary_data_t>();

	for (const auto it : *mov_items)
	{
		balance_item_data_t balance_item = (*balance_items)[it.first];

		summary_item_t& item_sum = (*summary)[it.first];
		item_sum.item = it.first;
		item_sum.balance_item = (*balance_items)[it.first];
		item_sum.last_amount = balance_item.get_init_amount();
		item_sum.last_value = balance_item.get_init_value();

		for (auto& day : it.second)
		{
			day_summary_item_t& day_sum = item_sum.day_sum[day.first];
			
			day_sum.date = day.second.date;
			day_sum.init_total_value = item_sum.last_value;
			day_sum.init_total_amount = item_sum.last_amount;
			day_sum.final_total_value = item_sum.last_value;
			day_sum.final_total_amount = item_sum.last_amount;

			for (const auto& mov_data : day.second.movs)
			{
				if (mov_data.type == "Ent")
				{
					day_sum.input_amount += mov_data.amount;
					day_sum.input_value += mov_data.value;
					day_sum.final_total_amount += mov_data.amount;
					day_sum.final_total_value += mov_data.value;
				}
				else if (mov_data.type == "Sai")
				{
					day_sum.output_amount += mov_data.amount;
					day_sum.output_value += mov_data.value;
					day_sum.final_total_amount -= mov_data.amount;
					day_sum.final_total_value -= mov_data.value;
				}
				else
					cout << "ERROR: invalid movement" << endl;
			}
			item_sum.last_value = day_sum.final_total_value;
			item_sum.last_amount = day_sum.final_total_amount;
		}
	}

	return summary;
}

void stock::print_summary(std::shared_ptr<summary_data_t> summary, 
	std::string filename) const
{
	FILE* f;
	f = fopen(filename.c_str(), "w");
	if (f)
	{
		fprintf(f,"Item;\
Data do lan�amento;\
Lan�amentos de Entrada: quantidade;\
Lan�amentos de Entrada: valor;\
Lan�amentos de Sa�da : quantidade;\
Lan�amentos de Sa�da: valor;\
Saldo inicial: quantidade;\
Saldo inicial: valor;\
Saldo final: quantidade;\
Saldo final: valor\n");

		for (auto& it : *summary)
		{
			summary_item_t& item = it.second;
			for (auto& d : item.day_sum)
			{
				day_summary_item_t& day = d.second;
				fprintf(f, "%s;%s;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f\n",
					item.item.c_str(),
					day.get_formated_date().c_str(),
					day.input_amount,
					day.input_value,
					day.output_amount,
					day.output_value,
					day.init_total_amount,
					day.init_total_value,
					day.final_total_amount,
					day.final_total_value);
			}
		}
		fclose(f);

		cout << "Summary output file: " << filename << endl;
	}
}